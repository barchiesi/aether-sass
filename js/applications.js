/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Swiper from 'swiper';
import '../css/kde-org/applications.scss';

let swiper = new Swiper('.swiper-container', {
  slidesPerView: 'auto',
  spaceBetween: 30,
  centeredSlides: true,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
    spaceBetween: 30,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

const search = document.getElementById('search');
if (search) {
  search.addEventListener('input', function(e) {
    const apps = document.querySelectorAll('.app');
    const regex = new RegExp(e.target.value, 'i');
    apps.forEach(function(app) {
      if (app.querySelector('img').title.search(regex) !== -1) {
        // app name is in search
        if (app.classList.contains('d-none')) {
          app.classList.remove('d-none');
        }
      } else if (!app.classList.contains('d-none')) {
        app.classList.add('d-none');
      }
    });

    const categories = document.querySelectorAll('.category');
    categories.forEach(function(category) {
      if (category.querySelectorAll('.app:not(.d-none)').length > 0 && category.classList.contains('d-none')) {
        category.classList.remove('d-none');
      } else if (category.querySelectorAll('.app:not(.d-none)').length === 0 && !category.classList.contains('d-none')) {
        category.classList.add('d-none');
      }
    });
  });
}
